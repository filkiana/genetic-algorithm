import random
class Point(object):
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

class Cell(object):
    def __init__(self, letters, points = set()):
        self.__points = set()
        self.__mem = ()

        mem = []
        self.__memset = set()
        if len(points) == 0:
            while(len(self.__points) < 5):
                self.__points.add(Point((int(random.random()*1000)%13 + 2), (int(random.random()*1000)%14 + 1)))
        else: 
            self.__points = points
        for l in range(0, 26):
            point_list = []
            for p in self.__points:
                point_list.append(letters[l][p.x][p.y])
            mem.append(tuple(point_list))
        self.__mem = tuple(mem)
        self.__memset = set(self.__mem)

        self.error = self.__Error(letters)
    def Predict(self, letter):
        mask = []
        for p in self.__points:
            mask.append(letter[p.x][p.y])
        mask = tuple(mask)
        same = [i for i, x in enumerate(self.__mem) if x == mask]
        l = len(same)
        for i in range(0, l):
            same[i] = chr(65 + int(same[i]))
        return 1/len(same), same
    #def __Predict(self, ):

    def Reproduct(self, cell, letters, mutation_rate):
        parent_a = self.__points.copy()
        parent_b = cell.__points.copy()
        points = set()
        while len(points) < 5:
            if(len(parent_a) == 0 or len(parent_b) == 0):
                points.add(Point((int(random.random() * 1000) % 16), (int(random.random() * 1000) % 14 + 1)))
                break
            gen_a = random.choice(list(parent_a))
            gen_b = random.choice(list(parent_b))
            parent_a.discard(gen_a)
            parent_b.discard(gen_b)
            points.add(gen_a)
            points.add(gen_b)
        gen_k = random.choice(list(points))
        points.discard(gen_k)
        if(random.random() < (mutation_rate)):
            gen_k = random.choice(list(points))
            points.discard(gen_k)
            points.add(Point((int(random.random()*1000)%16), (int(random.random()*1000)%14 + 1)))
        return Cell(letters, points)



        


    def GetPoints(self):
        return self.__points

    def __Error(self, letters):
        return 100 - len(self.__memset)/26 *100#26 - len(self.__memset)#
        '''
    def __Efficiency(self, index):
        min = 100
        z = 0
        for j in range(0, 26):
            p, s = self.__population[index].Predict(self.__letters[j])
            if min > p:
                min = p
            z = z + p**2
        return ((min**2 + z)/27)**(1/2)
    '''
    def Print(self):
        for i in range(0, 16):
            print()
            for j in range(0, 16):
                z = '-'
                for p in self.__points:
                    if i == p.x and j == p.y:
                       z = 'X' 
                print(z, end = ' ')
        print()



'''
    def Reproduct(self, cell, letters, mutation_rate):
        new_points = self.__points + cell.__points
        while len(new_points) != 5:
            j = int(random.random()*1000)%len(new_points)
            new_points.pop(j) 
            if(random.random() < (mutation_rate)):
                new_points.append(Point((int(random.random()*1000)%16), (int(random.random()*1000)%14 + 1)))
            if random.random() < mutation_rate/8*3:
                j = int(random.random()*1000)%len(new_points)
                x = random.randint(-1, 1)
                y = random.randint(-1, 1)
                new_points.append(Point(((new_points[j].x + x)%16), (new_points[j].y + y)%16))
    return Cell(letters, new_points)
'''

