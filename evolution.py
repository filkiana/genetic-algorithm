import random
class Evolution(object):
    def __init__(self, population_size, mutation_rate, selection_rate, data, pop_type, population = []):
        self.__mutation_rate = mutation_rate
        self.__population_size = population_size
        self.__population = population
        self.__selection_rate = selection_rate
        self.__data = data
        self.__pop_type = pop_type
        if len(population) == 0:
            for i in range(0, population_size):
                self.__population.append(pop_type(data))


    def __Selection(self):
        self.__population = sorted(self.__population, key = lambda pop_type: pop_type.error)
        selected = []
        for i in range(0, int(self.__selection_rate * self.__population_size) + 1):
            selected.append(self.__population[i])
        self.__population = selected
        return selected[0]

    def __GreatReplacement(self):
        self.__population = list(self.__population)
        s = self.__population_size * 0.01
        for _ in range(0, s):
            i = random.randint(0, self.__population_size * 0.01)
            self.__population.pop(self.__population[i])
            self.__population_size = self.__population_size - 1
            self.__population.append(pop_type(self.__data))
        self.__population = set(self.__population)

    def __Repruduction(self):
        new_population = set(tuple([self.__population[0]]))
        weights =[(1 / self.__population[i].error) for i in range(0, len(self.__population))]#[1/(1 + i**(1/20)) for i in range(0, len(self.__population))] #
        indexes = [i for i in range(0, len(self.__population))]
        while(len(new_population) < self.__population_size):
            i = random.choices(indexes, weights)[0]
            j = random.choices(indexes, weights)[0]
            new_population.add(self.__population[i].Reproduct(self.__population[j], self.__data, self.__mutation_rate))
        self.__population = new_population

    def Start(self, steps = 1, max_error = 0):
        best = self.__pop_type(self.__data)
        for i in range(0, steps):
            best = self.__Selection()
            #best.Print()
            #print(best.error)
            if best.error <= max_error:
                return best
            self.__Repruduction()
            self.__selection_rate = self.__selection_rate * 0.95
        return best

    def GetPop(self):
        return self.__population
