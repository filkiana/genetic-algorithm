Geneticky algoritmus.

To je program implementuje geneticky algoritmus pro vyšlechtění pozice peti bodu tak, 
aby bylo mozne podle hodnot techto peti bodu znaky latinky od sebe rozeznat s maximální presnosti.

*  Chromozomem je pole z 5 bodu. Kazdy bod ma hodnoty x, y a jsou mezi 0 a 15.
*  V kazde populace je hodne organismu s temito chromozomy.
*  Kazdy organismus ma pamet a v ni pamatuje jak vypadal ten znak latinky.
Pomatuje jenom ty 5 bodu.
*  Pri prvnim spusteni populace ty organismy nahodne se inicializuje.
Pak se spusti cyklus ve kterem po sobe jdou selekce, reprodukce, katastrofa 
a jsete teplota se meni pro simulovane zihani.(katastrofa a simulovane zihani)
*  Program spusti nekolik populace a pak spusti jeste jednu populace na zaklede 
vysledku predchozich. (ostrovni model)
* Pouzivam elitist selection.

1. Selekce a simulovane zihani.

  Seradi organismy podle efektivity a smazuje z populace vsech neefektivnich.
  Dela to pomoci *selection_rate*. Funguje to jako teplota. Na zacatku ma hodnotu 0,95. 
  To znamena ze z populace zustane 95% organismu. Pro kazdy dalsi krok cyklu
  plati t[i + 1] = t[i] * 0.95 ( t - teplota).
  
  Pamet organismu je mnozina a proto kdyz pro nekolik ruznych znaku pomatuje 
  stejnu mnozinu bodu pak pro neho nebude mezi temi znaky videt rozdil a velikost 
  touto mnoziny bude mene nez 26. Pak efektivita je velikost mnoziny pameti delene 26.

2. Reprodukce

  Z populace nahodne (pouzivam funkce distribuce pro nahodnou hodnotu. Efektivni 
  má vetsi sanci nez neefektivni) vybirame 2 organismy. Pak jich chromozomy
  se spojujou. Body z toho nahodne se smazujou dokud velikost neni 
  spravna(chromozom ma v sobe 5 bodu). Tady  prochazi mutace a jejich pravdepodobnost je *mutation_rate*. Ta reprodukce 
  jde dokud velikost populace mene nez musi byt(*population_size*). Z predchozi populace zustanou nejlepsi organismy(elitist selection).

3. Katastrofa

  Tady nahodne vybira organizmus a smazuje ho. Pak pridava uplne novy nahodny organismus.

Na konci se zobrazi efektivitu a umisteni bodu vysledku.



* 1.0
* ['A']
* 1.0
* ['B']
* 1.0
* ['C']
* 1.0
* ['D']
* 0.5
* ['E', 'F']
* 0.5
* ['E', 'F']
* 1.0
* ['G']
* 1.0
* ['H']
* 1.0
* ['I']
* 1.0
* ['J']
* 1.0
* ['K']
* 1.0
* ['L']
* 1.0
* ['M']
* 1.0
* ['N']
* 1.0
* ['O']
* 1.0
* ['P']
* 1.0
* ['Q']
* 1.0
* ['R']
* 1.0
* ['S']
* 1.0
* ['T']
* 1.0
* ['U']
* 1.0
* ['V']
* 1.0
* ['W']
* 1.0
* ['X']
* 1.0
* ['Y']
* 1.0
* ['Z']

" - - - - - - - - - - - - - - - -"

" - - - - - - - - - - - X - - - -"

" - - - - - - - - - - - - - - - -" 

" - - - - - X - - - - - - - - - - "

" - - - - - - - - - - - - - - - - "

" - - - - - - - - - - - - - - - - "

" - - - - - - - - - - - - - - - - "

" - - - - - - - - - - - - - - - - "

" - - - - - - X - - - - - - - - - "

" - - - - - - - - - - - - X - - - "

" - - - - - - - - - - - - - - - - "

" - - - - - - - - - - - - - - - - "

" - - - - - - - - - - - - - - - - "

" - - - - - - X - - - - - - - - - "

" - - - - - - - - - - - - - - - - "

" - - - - - - - - - - - - - - - - "

" 3.8461538461538396"



